# k8s-install
## Description 

K8s command for installation. 
Tested and approuved on Ubuntu 20.04 LTS. 

## Command 
### Context

```
lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 20.04.5 LTS
Release:        20.04
Codename:       focal
```

```
hostname
k8smaster
```

### Preparation

```bash
apt-get update && apt-get upgrade -y
```

```bash
apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common apt-transport-https ca-certificates uidmap -y
```
Disable swap otherwise the cluster don't init. 

```bash
swapoff -a
```
by default, the firewall is disabled. 

```
ufw disable
Firewall stopped and disabled on system startup
```

Load this twoo modules 

```bash
modprobe overlay
```

```bash
modprobe br_netfilter
```
Add this config 

```bash
cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
```
Check 

```bash
sysctl --system
```
Add key 

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
```

Return if good 

```bash
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
OK
```


```bash
apt install containerd -y
```
### K8s

```bash
vim /etc/apt/sources.list.d/kubernetes.list
```
add this 

```bash
deb http://apt.kubernetes.io/ kubernetes-xenial main
```

```bash
curl -s \
https://packages.cloud.google.com/apt/doc/apt-key.gpg \
| apt-key add -
```
return expected

```bash
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
OK
```
```bash
apt-get update
```
```bash
apt-get install -y kubeadm=1.24.1-00 kubelet=1.24.1-00 kubectl=1.24.1-00
```

```bash
apt-mark hold kubelet kubeadm kubectl
```

```bash
wget https://docs.projectcalico.org/manifests/calico.yaml
```

```bash
less calico.yaml
```

Check those lines and remmember 

```bash
   - name: FELIX_WIREGUARDMTU
              valueFrom:
                configMapKeyRef:
                  name: calico-config
                  key: veth_mtu
            # The default IPv4 pool to create on startup if none exists. Pod IPs will be
            # chosen from this range. Changing this value after installation will have
            # no effect. This should fall within `--cluster-cidr`.
            # - name: CALICO_IPV4POOL_CIDR
            #   value: "192.168.0.0/16"
            # Disable file logging so `kubectl logs` works.
```

Check your id address 

```
ip a 
```

```bash
enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 09:00:42:32:50:7c brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.76/24 metric 100 brd 192.168.1.255 scope global dynamic enp0s3
       valid_lft 3175sec preferred_lft 3175sec
```

```bash
 vim /etc/hosts
```

```bash
127.0.0.1       localhost
192.168.1.76 k8smaster
# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```
Create and add this config yaml for your cluster

```
vim kubeadm-config.yaml
```

**CHECK Your HOSTNAME and your CIDR . **

```bash
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.24.1 #<-- Use the word stable for newest version
controlPlaneEndpoint: "k8smaster:6443" #<-- Use the alias we put in /etc/hosts not the IP
networking:
  podSubnet: 192.168.1.0/16 #<-- Match the IP range from the Calico config file
```
Init your cluster

```bash
kubeadm init --config=kubeadm-config.yaml --upload-certs \
| tee kubeadm-init.out
```

For use with root user 

```bash
export KUBECONFIG=/etc/kubernetes/admin.conf
```

Enjoy 










